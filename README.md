# CPP-SDK #
This is an SDK for the cpp language allows easy integrating VAIS services into cpp applications:.

## Supported services
* Speech-to-text: __supported__
* Text-to-speech: underdevelopment
* NLP: underdevelopment

# Installing / Getting started
## Install prerequisites
```
sudo apt-get install cmake make g++ gcc git build-essential autoconf automake libtool
```

The SDK depends on `gRPC` that provides communication protocol with VAIS servers.
To install `gRPC`:
```
git clone -b v1.4.x https://github.com/grpc/grpc.git
cd grpc && git submodule update --init
make -j 4 && make install
cd third_party/protobuf && make install
```

## Building
### Building `vaislib`
```
cd vaislib && mkdir -p build && cd build
cmake ../src
make -j 4 && make install
```

### Running the example
* Speech-to-text
```
./bin/main_speech2text YOUR_API_KEY audio.wav
```

Registers your free API key [here](https://app.vais.vn/dev)
