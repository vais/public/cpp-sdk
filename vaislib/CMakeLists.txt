cmake_minimum_required (VERSION 2.8)
set(CMAKE_BUILD_TYPE Release)
PROJECT(vais-cpp-sdk)
set (vais-cpp-sdk_VERSION_MAJOR 0)   # when you make incompatible API changes
set (vais-cpp-sdk_VERSION_MINOR 1)   # when you add functionality in a backwards-compatible manner. Reset to 0 when MAJOR increased
set (vais-cpp-sdk_VERSION_PATCH 0)   # when you make backwards-compatible bug fixes. Reset to 0 when MINOR increased
set (vais-cpp-sdk_COPYRIGHT "Copyright 2016 VAIS. All right reserved.")   # when you make backwards-compatible bug fixes. Reset to 0 when MINOR increased

SET(CMAKE_CXX_FLAGS "-std=c++0x -std=c++11")
if(UNIX)
        SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -std=gnu++0x")
endif()

## Compiler flags
if(CMAKE_COMPILER_IS_GNUCXX)
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -O3")        ## Optimize
    set(CMAKE_EXE_LINKER_FLAGS "-s")  ## Strip binary
endif()

add_subdirectory (vais)
add_subdirectory (include)
add_subdirectory (bin)
