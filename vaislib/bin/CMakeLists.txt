INCLUDE_DIRECTORIES(${CMAKE_CURRENT_SOURCE_DIR}/../include)
find_package(Protobuf REQUIRED)
SET(CMAKE_EXE_LINKER_FLAGS "")
set(GENERATED_PROTOBUF_PATH "${CMAKE_BINARY_DIR}/generated")

find_package (Threads)

SET(CMAKE_EXE_LINKER_FLAGS
              "${CMAKE_EXE_LINKER_FLAGS} -Wl,-rpath -Wl,$ORIGIN")

include_directories(${GENERATED_PROTOBUF_PATH})
link_libraries(grpc++_unsecure grpc gpr ${PROTOBUF_LIBRARY})

add_executable(main_speech2text main_speech2text.cpp )

target_link_libraries( main_speech2text LINK_PUBLIC vais ${CMAKE_THREAD_LIBS_INIT} grpc++_unsecure grpc gpr ${PROTOBUF_LIBRARY})
install(TARGETS main_speech2text DESTINATION bin)
