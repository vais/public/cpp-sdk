/*
 * Copyright (C) 2017 VAIS
 *
 */

#include <sstream>
#include <iostream>
#include <string>
#include <thread>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <fstream>
#include <vais.h>

typedef struct header_file
{
    char chunk_id[4];
    int chunk_size;
    char format[4];
    char subchunk1_id[4];
    int subchunk1_size;
    short int audio_format;
    short int num_channels;
    int sample_rate;            // sample_rate denotes the sampling rate.
    int byte_rate;
    short int block_align;
    short int bits_per_sample;
    char subchunk2_id[4];
    int subchunk2_size; // subchunk2_size denotes the number of samples.
    //char data; // actual data : Added by tarmizi
} header;
typedef struct header_file* header_p;

using vais::cloud::speech::v1::StreamingRecognizeRequest;
using vais::cloud::speech::v1::RecognizeRequest;
using vais::cloud::speech::v1::RecognitionConfig;
using vais::cloud::speech::v1::StreamingRecognitionConfig;
using vais::cloud::speech::v1::RecognitionAudio;
using vais::cloud::speech::v1::SpeechContext;
using vais::cloud::speech::v1::RecognizeResponse;
using vais::cloud::speech::v1::StreamingRecognizeResponse;

int main(int argc, char** argv) {
  grpc::Status status;
	char * api_key =argv[1];
	FILE * infile = fopen(argv[2],"rb");
  VaisLib::VaisClient *vais_client = new VaisLib::VaisClient(api_key);

  header_p meta = (header_p)malloc(sizeof(header));

  size_t nread = fread(meta, 1, sizeof(header), infile);
  if (!nread){
    std::cerr << "Error reading file " << infile << std::endl;
    exit(1);
  }

  std::cout << "========= Audio Information =============="  << std::endl;
  std::cout << "First chunk is :" << sizeof(meta->chunk_id) << " bytes in size" << std::endl;
  std::cout << "The file is a :" << meta->chunk_id << " format" << std::endl;
  std::cout << "Size of Header file is "<<sizeof(*meta)<<" bytes" << std::endl;
  std::cout << "Sampling rate of the input wave file is "<< meta->sample_rate <<" Hz" << std::endl;
  std::cout << "Number of bits per sample is: "<< meta->bits_per_sample <<"bits" << std::endl;
  std::cout << "Size of data in the audio is: " << sizeof(meta->subchunk2_size)<< " bytes" << std::endl;
  std::cout << "The number of channels of the file is "<< meta->num_channels << " channels" << std::endl;
  std::cout << "The audio format is PCM:"<< meta->audio_format << std::endl;
  std::cout << "=========================================" << std::endl;

  std::cout << "Connecting ..." << std::endl;
  grpc::ClientContext context_asr;
  VaisLib::AsrStreamRequest streamer = vais_client->GetAsrStreamRequest(context_asr);
  StreamingRecognizeRequest request;
  auto* streaming_config = request.mutable_streaming_config();
  streaming_config->set_interim_results(true);
  streaming_config->set_single_utterance(true);
  auto* config = streaming_config->mutable_config();
  config->set_encoding(RecognitionConfig::LINEAR16);
  config->set_max_alternatives(10);
  config->set_enable_word_time_offsets(true);
  auto* speech_context = config->add_speech_contexts();
  speech_context->add_phrases("không một hai ba bốn năm sáu bảy tám chín mười");
  streamer->Write(request);


  /* Read audio and send it in a separate thread
      You can of course write this code in a single thread
      But you will have to wait for all audio data is sent before getting response */
  std::thread writer([streamer, infile]() {
    StreamingRecognizeRequest asr_request;

    size_t BUFSIZE = 640; // buffer size has to be 640
    char buff[BUFSIZE];
    size_t nb;
    while (!feof(infile)){
      nb = fread(buff, 1, BUFSIZE, infile);
      // Always send 640 bytes, pad with 0 if not enough data
      if (nb == BUFSIZE){
        asr_request.set_audio_content(buff, BUFSIZE);
        streamer->Write(asr_request);
      }
    }
    streamer->WritesDone();
  });
  std::cout << "Waiting for result" << std::endl;

  /* Reading responses returned by the server in readl-time.
    The final() function will return true if the text is finalized.
    The while loop will automatically break when the server close the connection */
  StreamingRecognizeResponse response;
  while (streamer->Read(&response)) {
    if (response.results_size()){
      auto result = response.results(0);
      if (result.alternatives_size()){
        auto alternative = result.alternatives(0);
        std::cout << "----------" << std::endl;
        std::cout << alternative.transcript() << std::endl;
        for (int i = 0; i < alternative.words_size(); ++i) {
          std::cout << alternative.words(i).start_time().seconds() << "." << alternative.words(i).start_time().nanos() << "\t" << alternative.words(i).end_time().seconds() << "." << alternative.words(i).end_time().nanos() << "\t" << alternative.words(i).word()  << std::endl;
        }
      }
    }
  }
  status = streamer->Finish();
  if (!status.ok()) {
    std::cerr << status.error_message() << std::endl;
    return -1;
  }
  writer.join();
  return 0;
}
