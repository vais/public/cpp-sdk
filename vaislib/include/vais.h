/*
 * vais.h
 * Copyright (C) 2017 truong-d <truongdo@vais.vn>
 *
 */

#ifndef VAIS_H
#define VAIS_H

#include <grpc/grpc.h>
#include <grpc++/channel.h>
#include <grpc++/client_context.h>
#include <grpc++/create_channel.h>
#include <grpc++/security/credentials.h>
#include "speech/v1/cloud_speech.grpc.pb.h"

namespace VaisLib{
  typedef std::shared_ptr<grpc::ClientReaderWriter<vais::cloud::speech::v1::StreamingRecognizeRequest, vais::cloud::speech::v1::StreamingRecognizeResponse> > AsrStreamRequest;
  class VaisClient {
    public:
      VaisClient(std::string api_key);
      AsrStreamRequest GetAsrStreamRequest(grpc::ClientContext&);
      //std::string MakeTtsRequest(std::string text);
    private:
      std::unique_ptr<vais::cloud::speech::v1::Speech::Stub> stub_;
      std::string api_key;
      std::shared_ptr<grpc::Channel> channel;
  };
}

#endif /* !VAIS_H */
