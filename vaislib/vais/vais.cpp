/*
 * vais.cpp
 * Copyright (C) 2017 truong-d <truong-d@X1Carbon>
 *
 * Distributed under terms of the MIT license.
 */

#include "vais.h"
namespace VaisLib{
  VaisClient::VaisClient(std::string api_key){
    this->api_key = api_key;
    if (api_key == ""){
      std::cerr << "API key is empty" << std::endl;
      exit(1);
    }
    channel = grpc::CreateChannel("service.grpc.vais.vn:50051", grpc::InsecureChannelCredentials());
    stub_ = vais::cloud::speech::v1::Speech::NewStub(channel);
  }

  AsrStreamRequest VaisClient::GetAsrStreamRequest(grpc::ClientContext& context_asr){
    context_asr.AddMetadata("api-key", api_key);
    AsrStreamRequest asr_request_stream(stub_->StreamingRecognize(&context_asr));
    return asr_request_stream;
  }
}
